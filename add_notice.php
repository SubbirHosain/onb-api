<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 7/3/2018
 * Time: 1:00 AM
 */

include 'database.php';
$dbh = new Database();

$response = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (isset($_POST['notice_title']) && $_POST['notice_content']) {

        $notice_title = $_POST['notice_title'];
        $notice_category_id = $_POST['notice_category'];
        $notice_dt = $_POST['notice_date'];

        $notice_featured_image = $_POST['notice_featured_image'];
        $notice_content = $_POST['notice_content'];

        date_default_timezone_set('Asia/Dhaka');
        $time = date('H:i:s');
        $notice_datetime = date("Y-m-d $time", strtotime($notice_dt));


        $sql = "INSERT INTO onb_notice (notice_title,notice_content,notice_category_id,notice_date,notice_featured_image) VALUES (?,?,?,?,?)";
        $data = array($notice_title, $notice_content, $notice_category_id, $notice_datetime, $notice_featured_image);
        $status = $dbh->insertRow($sql, $data);

        if ($status) {
            $response['notice_error'] = false;
            $response['notice_message'] = 'Published Successfully';
        }

    }

} else {
    $response['notice_error'] = true;
    $response['notice_message'] = 'Invalid Request...';
}

echo json_encode($response);
