<?php
class Database
{
    private $isConn;
    private $db_name = 'subbirho_onb';
    private $db_user = 'subbirho_retrofi';
    private $db_pass = '';
    private $db_host = 'localhost';
    private $char = 'utf8';
    private $dbh;

    public function __construct()
    {
        if (!isset($this->dbh)) {

            $dsn = 'mysql:host=' . $this->db_host . ';dbname=' . $this->db_name . ';charset=' . $this->char;

            $options = array(PDO::ATTR_PERSISTENT => true);

            try {
                $link = new PDO($dsn, $this->db_user, $this->db_pass, $options);
                $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $this->dbh = $link;
                $this->isConn = TRUE;
            } catch (PDOException $exc) {
                $errmsg = $exc->getMessage();
                echo "{'status':400,'message':$errmsg}";
            }
        }
    }


    public function Disconnect()
    {
        $this->dbh = NULL;
        $this->isConn = FALSE;
    }

    public function rowCounts($query, $params = [])
    {
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return $sth->rowCount();
        } catch (PDOException $exc) {
            $errmsg = $exc->getMessage();
            echo "{'status':400,'message':$errmsg}";
        }
    }

    public function getRow($query, $params = [])
    {
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return $sth->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $exc) {
            $errmsg = $exc->getMessage();
            echo "{'status':400,'message':$errmsg}";
        }
    }


    public function getRows($query, $params = [])
    {
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            //$result = [];
            //$result['data'] =

            return $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $exc) {
            $errmsg = $exc->getMessage();
            echo "{'status':400,'message':$errmsg}";
        }
    }


    public function insertRow($query, $params = [])
    {

        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return TRUE;
        } catch (PDOException $exc) {
            $errmsg = $exc->getMessage();
            echo "{'status':400,'message':$errmsg}";
        }
    }

    public function updateRow($query, $params = [])
    {
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return $sth->rowCount();
        } catch (PDOException $exc) {
            $errmsg = $exc->getMessage();
            echo "{'status':400,'message':$errmsg}";
        }
    }

    public function deleteRow($query, $params = [])
    {
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return TRUE;
        } catch (PDOException $exc) {
            $errmsg = $exc->getMessage();
            echo "{'status':400,'message':$errmsg}";
        }
    }


    public function registerDevice($email, $token, $date, $time)
    {
        if (!$this->isEmailExist($email)) {
            $sql = "INSERT INTO fcm_users (fcm_token, user_email, fcm_date, fcm_time) VALUES (?,?,?,?) ";
            $data = array($token, $email, $date, $time);
            $sth = $this->dbh->prepare($sql);
            if ($sth->execute($data)) {
                return 0;
            } else {
                return 1;
            }

        } else {
            $sql = "UPDATE fcm_users SET fcm_token = ?, fcm_date = ?, fcm_time = ? WHERE user_email = ?";
            $data = array($token, $date, $time, $email);
            $sth = $this->dbh->prepare($sql);
            if ($sth->execute($data)) {
                return 2;
            }
        }

    }

    private function isEmailexist($email)
    {
        $sql = "SELECT fcm_id FROM fcm_users WHERE user_email = ?";
        $data = array($email);
        $sth = $this->dbh->prepare($sql);
        $sth->execute($data);
        return $sth->rowCount();
    }

    public function registerUser($user_fullname, $user_email, $user_password, $user_session, $user_datetime)
    {
        if (!$this->isUserEmailexist($user_email)) {
            $sql = "INSERT INTO onb_user (user_fullname,user_email,user_password,user_session,user_datetime) VALUES (?,?,?,?,?)";
            $data = array($user_fullname, $user_email, $user_password, $user_session, $user_datetime);
            $sth = $this->dbh->prepare($sql);
            if ($sth->execute($data)) {
                return 0;
            } else {
                return 1;
            }

        } else {
            return 2;
        }
    }

    private function isUserEmailexist($email)
    {
        $sql = "SELECT user_email FROM onb_user WHERE user_email = ?";
        $data = array($email);
        $sth = $this->dbh->prepare($sql);
        $sth->execute($data);
        return $sth->rowCount();
    }

} ?>