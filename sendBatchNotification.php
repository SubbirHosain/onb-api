<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 7/7/2018
 * Time: 3:36 PM
 *
 * send fcm topic message
 */

if (isset($_POST['title'])) {
    require_once __DIR__ . '/notification.php';
    $notification = new Notification();

    $title = $_POST['title'];
    $message = isset($_POST['message']) ? $_POST['message'] : '';
    $actionDestination = isset($_POST['action_destination']) ? $_POST['action_destination'] : '';
	
	$imageUrl = isset($_POST['image_url'])?$_POST['image_url']:'';

    $notification->setTitle($title);
    $notification->setMessage($message);
	$notification->setImage($imageUrl);
    $notification->setActionDestination($actionDestination);


    $topic = $_POST['topic'];

    $firebase_api = "AAAA_KGJkuA:APA91bHilXRxOcazYf6l5_Q_syYUbGsDIlFVP6P6mEieZVcPS_tHqjZmGK083Cp_mcCQYog6Rj4Qwt6VHIH-Xl3NJMPN4O0H9N1HZjkUOACxi_Y5ISY4cZqM-7DxQwEIwAik_kkH9_ewW6olaVgw8gYIFlHp7z4K1g";
    $requestData = $notification->getNotificatin();

    $fields = array(
        'to' => '/topics/' . $topic,
        'data' => $requestData,
    );

    $url = 'https://fcm.googleapis.com/fcm/send';

    $headers = array(
        'Authorization: key=' . $firebase_api,
        'Content-Type: application/json'
    );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    $result = curl_exec($ch);
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);

    echo $result;
}