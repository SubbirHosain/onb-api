<?php
include('database.php');
$dbh = new Database();

// json response array
$response = array();

if ($_POST['email'] && $_POST['password']) {

    // receiving the post params
    $email = $_POST['email'];
    $password = md5($_POST['password']);

    // get the user by email and password
    $sql = "SELECT * FROM onb_user WHERE user_email = ? AND user_password= ? AND user_status = ?";
    $data = array($email, $password, 1);
    $result = $dbh->getRow($sql, $data);
    $num = $dbh->rowCounts($sql, $data);
    //if user found catch him
    if ($num > 0) {
        // user is found
        $response["login_error"] = FALSE;
        $response['user_email'] = $result['user_email'];
        $response['user_session'] = $result['user_session'];
        $response['user_id'] = $result['user_id'];
        $response['user_type'] = $result['user_type'];
        $response['login_message'] = "Successfully loggedin";
        echo json_encode($response);
    } else {
        // user is not found with the credentials
        $response["login_error"] = TRUE;
        $response["login_message"] = "Login credentials are wrong. Please try again!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["login_error"] = TRUE;
    $response["login_message"] = "Required parameters email or password is missing!";
    echo json_encode($response);
}
?>  
