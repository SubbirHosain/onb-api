<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 7/31/2018
 * Time: 3:35 PM
 */

include 'database.php';
$dbh = new Database();

$sql = "SELECT UNIX_TIMESTAMP(notice_date) as notice_timestamp FROM onb_notice WHERE notice_id = (SELECT notice_id FROM onb_notice ORDER BY notice_id DESC LIMIT 1 )";

$res = $dbh->getRow($sql);

$modified_time =  $res['notice_timestamp'];

// Get last modification time of the current PHP file
$file_last_mod_time = filemtime(__FILE__);

// Get last modification time of the main content (that user sees)
$content_last_mod_time = $modified_time;

// Combine both to generate a unique ETag for a unique content
// Specification says ETag should be specified within double quotes
$etag = '"' . $file_last_mod_time . '.' . $content_last_mod_time . '"';

// Set Cache-Control header
header('Cache-Control: max-age=120');

// Set ETag header
header('ETag: ' . $etag);


// Check whether browser had sent a HTTP_IF_NONE_MATCH request header
if(isset($_SERVER['HTTP_IF_NONE_MATCH'])) {
    // If HTTP_IF_NONE_MATCH is same as the generated ETag => content is the same as browser cache
    // So send a 304 Not Modified response header and exit
    if($_SERVER['HTTP_IF_NONE_MATCH'] == $etag) {
        header('HTTP/1.1 304 Not Modified', true, 304);
        exit();
    }
}

// Rest of the code in the PHP script
echo "hello world"."modified";


