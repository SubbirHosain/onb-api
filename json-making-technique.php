<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 7/3/2018
 * Time: 1:48 AM
 */

$posts_arr=array();
$posts_arr["posts"]=array();

while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {

    // this will map $row['name'] to just $name only
    extract($row);
    $formatted_time = date('j F, Y',$notice_timestamp);
    $post_item = array(
        "notice_id"=>$notice_id,
        "notice_title"=>$notice_title,
        "notice_content"=>$notice_content,
        "notice_photopath"=>$path_url.$notice_photoname,
        "notice_timestamp"=> $formatted_time
    );
    array_push($posts_arr['posts'], $post_item);
}

echo json_encode($posts_arr,JSON_UNESCAPED_UNICODE);


//selecting everything and making json object

$sql="select  id, name  from student ";
$row=$dbo->prepare($sql);
$row->execute();
$result=$row->fetchAll(PDO::FETCH_ASSOC);

//echo json_encode(array('data'=>$result));
echo json_encode($result);