<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 7/28/2018
 * Time: 10:50 AM
 */

include 'database.php';
$dbh = new Database();

$response = array();

$from_date = $_GET['from_date'];
$to_date = $_GET['to_date'];


$sql = "SELECT * FROM onb_notice WHERE STR_TO_DATE(notice_date, '%Y-%d-%m') BETWEEN ? AND ? ORDER by notice_id DESC";
$data = array($from_date, $to_date);

$status = $dbh->rowCounts($sql, $data);

if ($status > 0) {
    $result['status']=true;
    $result['notices'] = $dbh->getRows($sql, $data);
}


echo json_encode($result, JSON_UNESCAPED_UNICODE);




