<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 7/6/2018
 * Time: 10:11 AM
 *
 * will get all notices with pagination
 */


include 'database.php';
$dbh = new Database();

$sql = "SELECT UNIX_TIMESTAMP(notice_date) as notice_timestamp FROM onb_notice WHERE notice_id = (SELECT notice_id FROM onb_notice ORDER BY notice_id DESC LIMIT 1 )";

$res = $dbh->getRow($sql);

$modified_time =  $res['notice_timestamp'];

// Get last modification time of the current PHP file
$file_last_mod_time = filemtime(__FILE__);

// Get last modification time of the main content (that user sees)
$content_last_mod_time = $modified_time;

// Combine both to generate a unique ETag for a unique content
// Specification says ETag should be specified within double quotes
$etag = '"' . $file_last_mod_time . '.' . $content_last_mod_time . '"';

// Set Cache-Control header
header('Cache-Control: max-age=120');

// Set ETag header
header('ETag: ' . $etag);

header("Content-Type: application/json;charset=utf-8");

// Check whether browser had sent a HTTP_IF_NONE_MATCH request header
if(isset($_SERVER['HTTP_IF_NONE_MATCH'])) {
    // If HTTP_IF_NONE_MATCH is same as the generated ETag => content is the same as browser cache
    // So send a 304 Not Modified response header and exit
    if($_SERVER['HTTP_IF_NONE_MATCH'] == $etag) {
        header('HTTP/1.1 304 Not Modified', true, 304);
        exit();
    }
}

// Rest of the code in the PHP script

$page_number = $_GET['page_number'];
$item_count = $_GET['item_count'];

$from = $page_number*$item_count -($item_count-1);
$to = $page_number*$item_count;


$response = array();
$status = array();

$sql = "SELECT * FROM onb_notice";

$total_rows = $dbh->rowCounts($sql);

if ($to>$total_rows){
    
    array_push($response,array('status'=>'end'));
    echo json_encode($response,JSON_PRETTY_PRINT);
}
else{
    
    array_push($response,array('status'=>'ok'));
    $sql = "SELECT * FROM onb_notice LIMIT $from,$to";
    $notices = $dbh->getRows($sql);

    array_push($response,array('notices'=>$notices));
    sleep(2);
    echo json_encode($response,JSON_UNESCAPED_UNICODE);
}
