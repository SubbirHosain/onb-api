<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 7/6/2018
 * Time: 10:11 AM
 *
 * will get all notices with pagination
 */


include 'database.php';

$dbh = new Database();

$page_number = $_GET['page_number'];
$item_count = $_GET['item_count'];

$from = $page_number*$item_count -($item_count-1);
$to = $page_number*$item_count;


$response = array();
$status = array();

$sql = "SELECT * FROM onb_notice";

$total_rows = $dbh->rowCounts($sql);

if ($to>$total_rows){
    
    array_push($response,array('status'=>'end'));
    echo json_encode($response,JSON_PRETTY_PRINT);
}
else{
    
    array_push($response,array('status'=>'ok'));
    $sql = "SELECT * FROM onb_notice LIMIT $from,$to";
    $notices = $dbh->getRows($sql);

    array_push($response,array('notices'=>$notices));
    sleep(2);
    echo json_encode($response,JSON_UNESCAPED_UNICODE);
}
