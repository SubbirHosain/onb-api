<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 7/3/2018
 * Time: 12:35 AM
 * will upload the image and retrun that image
 */


$target_dir = "uploads/";
$target_file_name = $target_dir . basename($_FILES["file"]["name"]);
$response = array();

// Check if image file is a actual image or fake image
if (isset($_FILES["file"])) {
    if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file_name)) {
        //path url for image
        $object_url = getBaseUrl()."$target_file_name";
        $success = true;
        $upload_message = "Successfully Uploaded";

        $response['image_url']=$object_url;
        $response['success']=$success;
        $response['upload_message']=$upload_message;
    } else {
        $success = false;
        $upload_message = "Error while uploading";

        $response['success'] = $success;
        $response['upload_message']=$upload_message;

    }
} else {
    $success = false;
    $upload_message = "Required Field Missing";

    $response['success'] = $success;
    $response['upload_message']=$upload_message;
}

echo json_encode($response);



function getBaseUrl()
{
    // output: /myproject/index.php
    $currentPath = $_SERVER['PHP_SELF'];

    // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index )
    $pathInfo = pathinfo($currentPath);

    // output: localhost
    $hostName = $_SERVER['HTTP_HOST'];

    // output: http://
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';

    // return: http://localhost/myproject/
    return $protocol.$hostName.$pathInfo['dirname']."/";
}