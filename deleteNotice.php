<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 7/13/2018
 * Time: 3:12 PM
 */

include 'database.php';
$dbh = new Database();

$response = array();

if (isset($_GET['notice_id'])) {

    $notice_id = $_GET['notice_id'];

    $sql = "DELETE FROM onb_notice WHERE notice_id = ?";
    $data = array($notice_id);
    $status = $dbh->deleteRow($sql, $data);
    if ($status) {
        $response['delete_error'] = false;
        $response['delete_message'] = "successfully deleted";
    } else {
        $response['delete_error'] = true;
        $response['delete_message'] = "Failed to delete notice!";
    }
} else {
    $response['delete_error'] = true;
    $response['delete_message'] = "Invalid Noitce ID Request!";
}
echo json_encode($response);