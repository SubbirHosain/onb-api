<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 7/13/2018
 * Time: 4:49 PM
 */
include 'database.php';
$dbh = new Database();

$response = array();

if (isset($_GET['notice_id'])) {

    $notice_id = $_GET['notice_id'];

    $sql = "SELECT * FROM onb_notice WHERE notice_id = ?";

    $data = array($notice_id);
    $status = $dbh->rowCounts($sql, $data);

    if ($status){

        $result = $dbh->getRow($sql,$data);
        $response['notice_error'] = false;
        $response['notice_content'] = $result;
    }


} else {
    $response['notice_error'] = true;
    $response['notice_content'] = "No Notice available";
}
echo json_encode($response,JSON_UNESCAPED_UNICODE);