<?php 

   include "config.php";
   $response = array();
    try {
        
        //select all query
        $sql =  "SELECT * FROM onb_notices ORDER BY notice_id DESC LIMIT 9"; 
        //prepare query statement
        $sth = $dbh->prepare($sql);
        //execute query
        $sth->execute();
        //$result = $sth->fetchAll(PDO::FETCH_ASSOC);
        /*
            var_dump($result);
            echo json_encode($result);
            echo json_encode(array('data'=>$result));
        */
        //show result if row exists otherwise do nothing
        $num = $sth->rowCount();
        // check if more than 0 record found
        if($num>0)
        {
            //for loop to use array_push
            //$response['success']=1;
            //$response['posts']=$result;
            // posts array
            $posts_arr=array();
            $posts_arr["posts"]=array();
            // retrieve our table contents
            // fetch() is faster than fetchAll()
            // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
            
            //path url for image
            $path_url = getBaseUrl()."uploads/photos/";

             while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
                 // extract row
                // this will make $row['name'] to
                // just $name only
                extract($row);
                $formatted_time = date('j F, Y',$notice_timestamp);
                $post_item = array(
                    "notice_id"=>$notice_id,
                    "notice_title"=>$notice_title,
                    "notice_content"=>$notice_content,
                    "notice_photopath"=>$path_url.$notice_photoname,
                    "notice_timestamp"=> $formatted_time
                    );   
                array_push($posts_arr['posts'], $post_item);
             }

            echo json_encode($posts_arr,JSON_UNESCAPED_UNICODE);
        }
        else{
            $response['error']=0;
            $response['msg']="No Posts found";
            echo json_encode(array('postlist'=>$response));
        }
    } catch (Exception $e) {
        echo "An errror occured ".$e->getMessage();
    }    

   
function getBaseUrl() 
{
    // output: /myproject/index.php
    $currentPath = $_SERVER['PHP_SELF']; 
    
    // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index ) 
    $pathInfo = pathinfo($currentPath); 
    
    // output: localhost
    $hostName = $_SERVER['HTTP_HOST']; 
    
    // output: http://
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
    
    // return: http://localhost/myproject/
    return $protocol.$hostName.$pathInfo['dirname']."/";
}