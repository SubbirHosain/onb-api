<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 7/13/2018
 * Time: 9:11 PM
 */

include 'database.php';
$dbh = new Database();

$response = array();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    if (isset($_GET['notice_id'])) {

        if (isset($_GET['notice_title']) && $_GET['notice_content']) {

            $notice_id = $_GET['notice_id'];
            $notice_title = $_GET['notice_title'];
            $notice_category_id = $_GET['notice_category'];
            $notice_dt = $_GET['notice_date'];

            $notice_featured_image = $_GET['notice_featured_image'];
            $notice_content = $_GET['notice_content'];

            date_default_timezone_set('Asia/Dhaka');
            $time = date('H:i:s');
            $notice_datetime = date("Y-m-d $time", strtotime($notice_dt));

            $sql = "UPDATE onb_notice SET notice_title=?,notice_content=?,notice_category_id=?,notice_date=?,notice_featured_image=? WHERE notice_id=?";
            $data = array($notice_title, $notice_content, $notice_category_id, $notice_datetime, $notice_featured_image, $notice_id);

            $status = $dbh->updateRow($sql,$data);

            if ($status>0) {
                $response['notice_error'] = false;
                $response['notice_message'] = 'Updated Successfully';
            }
            else{
                $response['notice_error'] = false;
                $response['notice_message'] = 'Not Updated!';
            }

        }
    }
    else{
        $response['notice_error'] = true;
        $response['notice_message'] = 'No Notice Available!';
    }

}
else {
    $response['notice_error'] = true;
    $response['notice_message'] = 'Invalid Request...';
}

echo json_encode($response);
