<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 7/3/2018
 * Time: 1:21 AM
 * This file collects all the batch known as category
 */

include 'database.php';
$dbh = new Database();

$response = array();

$sql = "SELECT * FROM onb_category";

$result = $dbh->getRows($sql);

echo json_encode(array('category'=>$result),JSON_PRETTY_PRINT);