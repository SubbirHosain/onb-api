<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 7/2/2018
 * Time: 11:33 PM
 */
include 'database.php';

$response = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {


    if (isset($_POST['user_email']) && isset($_POST['user_password'])) {

        $fname = trim($_POST['fname']);
        $lname = trim($_POST['lname']);

        $user_fullname = $fname . " " . $lname;
        $user_email = $_POST['user_email'];
        $user_password = md5($_POST['user_password']);
        $user_session = $_POST['user_session'];

        date_default_timezone_set('Asia/Dhaka');
        $time = date('H:i:s');
        $user_datetime = date("Y-m-d $time");


        $db = new Database();

        $result = $db->registerUser($user_fullname, $user_email, $user_password, $user_session, $user_datetime);
        if ($result == 0) {
            $response['register_error'] = false;
            $response['register_message'] = 'User Registered successfully';
        } elseif ($result == 2) {
            $response['register_error'] = true;
            $response['register_message'] = 'User already Registered';
        } else {
            $response['register_error'] = true;
            $response['register_message'] = 'User not Registered';
        }

    }

} else {
    $response['register_error'] = true;
    $response['register_message'] = 'Invalid Request...';
}

echo json_encode($response);