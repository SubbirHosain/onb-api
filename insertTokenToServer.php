<?php 

include 'database.php';

$response = array();

if ($_SERVER['REQUEST_METHOD']=='POST') {

	$token = $_POST['token'];
	$email = $_POST['email'];

    date_default_timezone_set('Asia/Dhaka');
	$date = date("Y-m-d");
	$time = date('h:i:s');

	$db = new Database(); 

	$result = $db->registerDevice($email,$token,$date,$time);

	if($result == 0){
		$response['fcm_error'] = false;
		$response['fcm_message'] = 'Device registered successfully';
	}elseif($result == 2){
		$response['fcm_error'] = true;
		$response['fcm_message'] = 'Device token Updated';
	}else{
		$response['fcm_error'] = true;
		$response['fcm_message']='Device not registered';
	}	


} else{
	$response['fcm_error']=true;
	$response['fcm_message']='Invalid Request...';
}

echo json_encode($response);